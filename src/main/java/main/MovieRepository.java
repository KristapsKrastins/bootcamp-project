package main;

import java.util.List;

import javax.persistence.Column;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

//import main.Movie;

// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete

public interface MovieRepository extends CrudRepository<Movie, Long> {
	
	
	
	@Transactional
	@Modifying
    @Query("update movie m set m.name = ?1, m.description = ?2, m.posterLink = ?3, m.trailerLink = ?4 where m.id = ?5")
    void updateMovie(String name, String description,String posterLink, String trailerLink,  long id);
	
	@Transactional
	@Modifying
    @Query("update movie m set m.active = ?1 where m.id = ?2")
    void updateMovieActive(boolean active, long id);
	

}