package main;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name = "cinema_schedule")
public class CinemaSchedule {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private long id;

	private Timestamp timeFrom;
	private Timestamp timeTo;

	@ManyToOne
	@JoinColumn(name = "cinemaId")
	private Cinema cinema;

	@ManyToOne
	@JoinColumn(name = "auditoriumId")
	private CinemaAuditorium auditorium;

	@ManyToOne
	@JoinColumn(name = "movieId")
	private Movie movie;
	
	private boolean active ;

	CinemaSchedule() {
	} // JPA only

	public CinemaSchedule(Timestamp timeFrom, Timestamp timeTo, CinemaAuditorium auditorium, Cinema cinema,
			Movie movie) {
		setTimeFrom(timeFrom);
		setTimeTo(timeTo);
		setCinema(cinema);
		setAuditorium(auditorium);
		setMovie(movie);
		this.active = true;
	}

	public long getId() {
		return id;
	}

	public Timestamp getTimeFrom() {
		return timeFrom;
	}

	public void setTimeFrom(Timestamp timeFrom) {
		if (timeFrom.after(new Timestamp(System.currentTimeMillis()))) {
			this.timeFrom = timeFrom;
		} else {
			this.timeFrom = new Timestamp(0);
			//throw new RuntimeException("Can't schedule a movie in the past.");
		}
	}
	public void setToTime(Timestamp timeTo) {
		if (!timeTo.before(timeFrom) && !timeTo.equals(timeFrom)) {
			this.timeTo = timeTo;
		} else {			
			this.timeTo = new Timestamp(0);
			//throw new RuntimeException("Can't schedule movie end time before movie start time or both at the same time");
		}
	}

	public Timestamp getTimeTo() {
		return timeTo;
	}

	public void setTimeTo(Timestamp timeTo) {
//		if (timeTo.after(new Timestamp(System.currentTimeMillis()))) {
//			this.timeTo = timeTo;
//		} else {
//			throw new RuntimeException("Can't schedule a movie in the past.");
//		}
		if (!timeTo.before(timeFrom) && !timeTo.equals(timeFrom)) {
			this.timeTo = timeTo;
		} else {			
			this.timeTo = new Timestamp(0);
			//throw new RuntimeException("Can't schedule movie end time before movie start time or both at the same time");
		}
		
	}

	public void setId(long id) {
		this.id = id;
	}

	public Cinema getCinema() {
		return cinema;
	}

	public void setCinema(Cinema cinema) {
		this.cinema = cinema;
	}

	public CinemaAuditorium getAuditorium() {
		return auditorium;
	}

	public void setAuditorium(CinemaAuditorium auditorium) {
		this.auditorium = auditorium;
	}

	public Movie getMovie() {
		return movie;
	}

	public void setMovie(Movie movie) {
		this.movie = movie;
	}

	public static String[] getKeys() {
		return new String[] { "cinema name", "auditorium name", "movie", "Time from", "Time to" };
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

}
