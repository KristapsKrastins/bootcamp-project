package main;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Entity
public class Cinema {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private long id;
	private String name;
	private String city;
	private String address;
	private String imgUrl;
	private boolean active;



	public Cinema() {

	}

	public Cinema(String name, String city, String address, String imgUrl) {

//		this.name = name;
//		this.city = city;
//		this.address = address;
//		this.imgUrl = imgUrl;
		setName(name);
		setCity(city);
		setAddress(address);
		setImgUrl(imgUrl);

		this.active = true;

	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {

//		if(name == "" || name.equals(null)) {
//			this.name = "Not set";
//		}else {
//			this.name = name;
//		}
		if (name != null && !name.isEmpty()) {

			this.name = name;
		}else {
			this.name = "Not set";
		}
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		if (city != null && !city.isEmpty()) {
			this.city = city;
		} else {
			this.city = "Not set";
		}
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		if (address != null && !address.isEmpty()) {
			this.address = address;
		} else {
			this.address = "Not set";
		}
	}

	public String toString() {
		return String.format("Cinema[id=%d, name='%s', city='%s',address='%s']", id, name, city, address);
	}

	public static String[] getKeys() {
		return new String[] { "id", "name", "city", "address", "imgUrl" };
	}

	public String getImgUrl() {
		return imgUrl;
	}

	public void setImgUrl(String imgUrl) {
		if (imgUrl != null && !imgUrl.isEmpty()) {
			this.imgUrl = imgUrl;
		} else {
			this.imgUrl = "Not set";
		}
	}
	public boolean isActive() {
		return active;
	}

	public Cinema setActive(boolean active) {
		this.active = active;
		return this;
	}
}
