package main;

import java.io.IOException;
import java.io.InputStream;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

@Service
public class EmailService {

	@Autowired
	private JavaMailSender mailSender;

	@Autowired
	private TemplateEngine templateEngine;

	@Value("${main.mail.sender}")
	private String emailFromAddress;

	@Value("${main.purchase.timeout}")
	private Long purchaseTimeoutMinutes;
	
	@Async
	@Transactional
	public void sendPaymentLink(Purchase purchase) {
		final Context ctx = new Context(new Locale("lv-LV"));
		SimpleDateFormat dateTimeFormatter = new SimpleDateFormat("MMMM d, yyyy - H:mm");
		ctx.setVariable("purchase", purchase);
		ctx.setVariable("seatsNum", purchase.getSeats().size());
		ctx.setVariable("screeningDateTime", dateTimeFormatter.format(purchase.getSchedule().getTimeFrom()));
		ctx.setVariable("paymentTimeoutDateTime",
				dateTimeFormatter.format(new Date(System.currentTimeMillis() + purchaseTimeoutMinutes * 60 * 1000)));
		ctx.setVariable("paymentLink", "http://localhost:8080/purchase/" + purchase.getId().toString());

		final String txtContent = templateEngine.process("emails/payment-link.txt", ctx);

		final MimeMessage mimeMessage = mailSender.createMimeMessage();
		final MimeMessageHelper message = new MimeMessageHelper(mimeMessage, "UTF-8");
		try {
			message.setFrom(purchase.getSchedule().getCinema().getName() + "<" + emailFromAddress + ">");
			message.setTo(purchase.getUser().getName() + "<" + purchase.getUser().getEmail() + ">");
			message.setSubject("Your tickets purchase is pending payment");
			message.setText(txtContent);
			mailSender.send(mimeMessage);
		} catch (MessagingException | MailException e) {
			System.out.println(e.getMessage());
		}
	}

	@Async
	@Transactional
	public void sendInvoice(Purchase purchase) {
		final Context ctx = new Context(new Locale("lv-LV"));
		SimpleDateFormat dateTimeFormatter = new SimpleDateFormat("MMMM d, yyyy - H:mm");
		ctx.setVariable("purchase", purchase);
		ctx.setVariable("screeningDateTime", dateTimeFormatter.format(purchase.getSchedule().getTimeFrom()));
		ctx.setVariable("paymentLink", "http://localhost:8080/purchase/" + purchase.getId().toString());
		final String txtContent = templateEngine.process("emails/invoice-link.txt", ctx);

		try {
			final MimeMessage mimeMessage = mailSender.createMimeMessage();
			final MimeMessageHelper message = new MimeMessageHelper(mimeMessage, true, "UTF-8");
//			PipedOutputStream outputStream = new PipedOutputStream();
//			InputStream inputStream = new PipedInputStream(outputStream);
//			Document document = new Document();
//			PdfWriter.getInstance(document, outputStream);
			
//			document.open();
//			document.add(new Paragraph("Your tickets for \"" + purchase.getSchedule().getMovie().getName() + "\""));
//			document.add(new Paragraph("Show time: " + purchase.getSchedule().getTimeFrom()));
//			document.add(new Paragraph("Cinema: " + purchase.getSchedule().getCinema().getName()));
//			document.add(new Paragraph("Purchase ID: " + purchase.getId()));
//			document.add(new Paragraph("Total amount paid: " + purchase.getTotalAmountFormatted()));
//			document.add(new Paragraph("\n\n"));
//			for (Seat seat : purchase.getSeats()) {
//				document.add(new Paragraph("Row: " + seat.getRowNum().toString() + ", seat: " + seat.getSeatNum().toString()));
//			}
//			document.close();
	        
			message.setFrom(purchase.getSchedule().getCinema().getName() + "<" + emailFromAddress + ">");
			message.setTo(purchase.getUser().getName() + "<" + purchase.getUser().getEmail() + ">");
			message.setSubject("Your tickets for " + purchase.getSchedule().getMovie().getName());
			message.setText(txtContent);
//			message.addAttachment("invoice.pdf", new ByteArrayResource(IOUtils.toByteArray(inputStream)));
			mailSender.send(mimeMessage);
		} catch (MessagingException | MailException /*| IOException | DocumentException*/ e) {
			System.out.println(e.getMessage());
		}
	}
}
