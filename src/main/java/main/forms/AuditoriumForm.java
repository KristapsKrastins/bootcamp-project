package main.forms;

import javax.validation.constraints.Digits;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class AuditoriumForm {
	
	
	
	@NotNull(message="Cannot be empty")
    @Pattern(message = "Only letters allowed",regexp = "^[A-Za-z0-9 _]*[A-Za-z0-9][A-Za-z0-9 _]*$")
	private String name;
	
	
	@NotNull(message="Cannot be empty")
	private Integer cinemaId;
	
	@NotNull(message="Cannot be empty")
	@Min(value = 1)
	@Max(value = 99)
	private int rows;
	
	
	
	@NotNull(message="Cannot be empty")
	@Min(value = 1)
	@Max(value = 99)
	private int seatsPerRow;



	public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}



	public Integer getCinemaId() {
		return cinemaId;
	}



	public void setCinemaId(Integer cinemaId) {
		this.cinemaId = cinemaId;
	}



	public Integer getRows() {
		return rows;
	}



	public void setRows(Integer rows) {
		this.rows = rows;
	}



	public Integer getSeatsPerRow() {
		return seatsPerRow;
	}



	public void setSeatsPerRow(Integer seatsPerRow) {
		this.seatsPerRow = seatsPerRow;
	}
	
	
	

}