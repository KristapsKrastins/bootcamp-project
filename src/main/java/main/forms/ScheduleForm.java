package main.forms;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.validation.constraints.NotNull;

import org.apache.tomcat.jni.Time;



public class ScheduleForm {
	
	
	// public CinemaSchedule(Timestamp dateTime, Cinema cinema, CinemaAuditorium auditorium, Movie movie) {
	
	private String date;
	private String timeFrom,timeTo;

	@NotNull(message="Cannot be empty")
	private int cinemaID;
	
	@NotNull(message="Cannot be empty")
	private int auditoriumID;

	@NotNull(message="Cannot be empty")
	private int movieID;
	
	
	DateFormat sdf = new SimpleDateFormat("hh:mm");

	public int getCinemaID() {
		return cinemaID;
	}

	public void setCinemaID(int cinemaID) {
		this.cinemaID = cinemaID;
	}

	public int getAuditoriumID() {
		return auditoriumID;
	}

	public void setAuditoriumID(int auditoriumID) {
		this.auditoriumID = auditoriumID;
	}

	public int getMovieID() {
		return movieID;
	}

	public void setMovieID(Integer movieID) {
		this.movieID = movieID;
	}
	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		System.out.println("error");
		this.date = date;
	}


	public String getTimeFrom() {
		return timeFrom;
	}

	public void setTimeFrom(String timeFrom) {
		this.timeFrom = timeFrom;
	}

	public String getTimeTo() {
		return timeTo;
	}

	public void setTimeTo(String timeTo) {
		this.timeTo = timeTo;
	}
	
	
	

}