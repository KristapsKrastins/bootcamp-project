package main.forms;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import org.springframework.validation.annotation.Validated;

public class UserForm {
	
	
	
	@NotNull(message="Cannot be empty")
    @Pattern(message = "Only letters allowed",regexp = "^[A-Za-z _]*[A-Za-z][A-Za-z _]*$")
    private String name;
	
	@NotNull(message="Cannot be empty")
    @Pattern(message = "Input a proper e-mail address",regexp = "^(([^<>()\\[\\]\\\\.,;:\\s@\"]+(\\.[^<>()\\[\\]\\\\.,;:\\s@\"]+)*)|(\".+\"))@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$")
    private String email;
    


	@NotNull(message="Cannot be empty")
    @Pattern(message = "Only letters and numbers allowed",regexp = "^[A-Za-z0-9 _]*[A-Za-z0-9][A-Za-z0-9 _]*$")
    private String password;
    
	@NotNull(message="Cannot be empty")
	//@Size(message="Only 1 character allowed",min=1,max=1)
	//@Pattern(message = "Only values 'a' or 'u' allowed",regexp = "a")
    private char role;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Character getRole() {
		return role;
	}

	public void setRole(Character role) {
		this.role = role;
	}
	
	
}
