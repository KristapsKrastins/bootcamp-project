package main;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import main.Purchase;
import main.PurchaseRepository;
import main.exceptions.ConflictException;
import main.exceptions.NotFoundException;
import main.forms.PaymentForm;
import main.forms.PurchaseForm;

@Controller
@RequestMapping(path = "/purchase")
public class PurchaseController implements WebMvcConfigurer {
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private PurchaseRepository purchaseRepository;
	@Autowired
	private SeatReservationRepository seatReservationRepository;
	@Autowired
	private ScheduleRepository scheduleRepository;
	@Autowired
	private EmailService emailService;

	@PostMapping(path = "/pay/init")
	public String initPayment(@Valid PurchaseForm purchaseForm, BindingResult bindingResult) {
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (!auth.isAuthenticated() || auth.getName().equals("anonymousUser")) {
			return "redirect:/login";
		}

		if (bindingResult.hasErrors()) {
			throw new RuntimeException("Invalid purchase form");
		}
		Optional<CinemaSchedule> scheduleSearch = scheduleRepository.findById(purchaseForm.getScheduleId());
		Optional<User> userSearch = userRepository.findByEmail(auth.getName()); 
		if (scheduleSearch.isPresent() && userSearch.isPresent()) {
			CinemaSchedule schedule = scheduleSearch.get();
			Purchase purchase = new Purchase(userSearch.get(), schedule, new HashSet<Seat>(purchaseForm.getSeats()));
			purchaseRepository.save(purchase);
			for (Seat seat : purchaseForm.getSeats()) {
				Optional<SeatReservation> reservationSearch = seatReservationRepository.findByScreeningAndSeat(schedule, seat);
				if (reservationSearch.isPresent()) {
					throw new ConflictException("The seat was just taken by other user: row " + seat.getRowNum() + ", seat " + seat.getSeatNum() + ".");
				}
				SeatReservation reservation = new SeatReservation(schedule, seat, purchase);
				seatReservationRepository.save(reservation);
			}
			emailService.sendPaymentLink(purchase);
			return "redirect:/purchase/" + purchase.getId().toString();
		} else if (!scheduleSearch.isPresent()) {
			throw new NotFoundException("Screening not found");
		} else {
			throw new NotFoundException("Session not found");
		}

	}

	@PostMapping(path = "/pay/complete")
	public String completePayment(@Valid PaymentForm paymentForm, BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			throw new RuntimeException("Payment failed");
		}
		Optional<Purchase> purchaseSearch = purchaseRepository.findById(paymentForm.getPurchaseId());
		if (purchaseSearch.isPresent()) {
			Purchase purchase = purchaseSearch.get();
			if (purchase.getTotalAmount() == paymentForm.getAmountPaid()) {
				purchase.setStatus('p');
				purchase.setGatewayTransactionId(paymentForm.getGatewayTransactionId());
				purchaseRepository.save(purchase);
				emailService.sendInvoice(purchase);
				return "redirect:/purchase/" + purchase.getId().toString();
			} else {
				throw new NotFoundException("You're a cheater and didn't pay the exact expected amount.");
			}
		} else {
			throw new NotFoundException("Purchase ID not found, we've just solen your money.");
		}

	}

	@GetMapping(path = "/{purchaseId}")
	public String getPurchase(@PathVariable Long purchaseId, PaymentForm paymentForm, Model model) {
		Optional<Purchase> purchaseSearch = purchaseRepository.findById(purchaseId);
		if (purchaseSearch.isPresent()) {
			Purchase purchase = purchaseSearch.get();
			model.addAttribute("purchase", purchase);
			model.addAttribute("purchaseStatus", String.valueOf(purchase.getStatus()));
			Date dateTime = new Date(purchase.getSchedule().getTimeFrom().getTime());
			String formattedDateTime = new SimpleDateFormat("MMMM d, yyyy - H:mm").format(dateTime);
			model.addAttribute("dateTime", formattedDateTime);
			if (purchase.getStatus() == 'r') {
				paymentForm.setPurchaseId(purchase.getId());
				paymentForm.setAmountPaid(purchase.getTotalAmount());
				paymentForm.setGatewayTransactionId("trId:bootcamp");
			}
			return "fragments/purchase/purchase";
		} else {
			throw new NotFoundException("Cannot find purchase with ID: " + purchaseId.toString());
		}
	}

}
