package main;


import java.util.ArrayList;
import java.util.List;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import main.Cinema;
import main.CinemaRepository;


@Controller
@RequestMapping(path = "/cinema")
public class CinemaController {
	
	@Autowired
	private CinemaRepository cinemaRepository;
	@Autowired
	private MovieRepository movieRepository;
	@Autowired
	private ScheduleRepository scheduleRepository;;

	@GetMapping(path = "/add")
	public @ResponseBody String addNewCinema(@RequestParam String name, @RequestParam String city,
			@RequestParam String address, @RequestParam String ImgUrl) {
		Cinema cinema = new Cinema();
		cinema.setName(name);
		cinema.setCity(city);
		cinema.setAddress(address);
		cinema.setImgUrl(ImgUrl);
		cinemaRepository.save(cinema);
		Log.logger.debug("Cinema with id:" + cinema.getId() + " | name: " + cinema.getName()+ " in: " + cinema.getCity() +"has been added");
		return "Information has been saved!";
	}

		
		/**SHOWS MOVIES AND SCHEDULE FOR CINEMA**/
		
		@GetMapping(path="/{cinemaId}")
		public String displayCinema(@PathVariable long cinemaId,Model model) {
			
			//SHOWS MOVIES FOR CINEMA
				List<CinemaSchedule> cinemaSearchResults = scheduleRepository.findByCinemaId(cinemaId);
				List<Movie> moviesInCinema = new ArrayList<>();
				
				for (CinemaSchedule sch : cinemaSearchResults) {
					Movie m = sch.getMovie();
					
					if(!moviesInCinema.contains(m)) {
						moviesInCinema.add(m);
					}					
					
				}
				
			//SHOWS SCHEDULE FOR CINEMA

				Optional<Cinema> cinemaSearch = cinemaRepository.findById(cinemaId);
				
				if(cinemaSearch.isPresent()) {
					Cinema cinema = cinemaSearch.get();
					List<CinemaSchedule> movieSearchResults = scheduleRepository.findByCinemaIdAndActive(cinemaId,true);		
																					
					model.addAttribute("schedules", movieSearchResults);
					model.addAttribute("moviesInCinema", moviesInCinema);
					model.addAttribute("cinemaId", cinemaRepository.findById(cinemaId).get().getId());
					model.addAttribute("cinemaName", cinemaRepository.findById(cinemaId).get().getName());
					model.addAttribute("cinema", cinemaSearch.get());
					return "cinema";
				}
				else {
					Log.logger.debug("Cinema was not found");
					throw new RuntimeException("Cinema not found!");
				}
		}							
}
