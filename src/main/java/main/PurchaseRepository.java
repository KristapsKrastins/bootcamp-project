package main;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import main.Purchase;

public interface PurchaseRepository extends CrudRepository<Purchase, Long> {
	List<Purchase> findByStatus(char status);
}
