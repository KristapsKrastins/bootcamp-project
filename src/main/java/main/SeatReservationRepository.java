package main;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

public interface SeatReservationRepository extends CrudRepository<SeatReservation, Long> {
	
	List<SeatReservation> findByScreening(CinemaSchedule screening);

	Optional<SeatReservation> findByScreeningAndSeat(CinemaSchedule screening, Seat seat);

	List<SeatReservation> findByPurchase(Purchase purchase);

}
