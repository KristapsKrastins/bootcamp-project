package main;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

public interface ScheduleRepository extends CrudRepository<CinemaSchedule, Long> {
	List<CinemaSchedule> findByCinemaId(long cinemaId);
	
	@Query(value = "SELECT * FROM cinema_schedule WHERE cinema_id = ?1", nativeQuery = true)
	List<CinemaSchedule> findAllByCinemaId(long cinemaId);
	
	List<CinemaSchedule> findByTimeFromLessThanAndActive(Timestamp timeFrom, boolean active);

	List<CinemaSchedule> findByMovieId(long movieId);

	List<CinemaSchedule> findByCinemaIdAndMovieId(long cinemaId, long movieId);
	
	List<CinemaSchedule> findByCinemaIdAndActive(long cinemaId, boolean active);
	
	@Transactional
	@Modifying
	@Query(value = "update cinema_schedule CS "
    		+"inner join cinema C on CS.cinema_id = C.id "
    		+"inner join cinema_auditorium CA on CS.auditorium_id = CA.id "
    		+"inner join movie M on CS.movie_id = M.id "
    		+ "set CS.active =?1 "
    		+ "where C.active = 1 and M.active = 1 and CA.active=1 and CS.id = ?2 ", nativeQuery = true)
    void updateScheduleActive(boolean active, long id);
	
	
	@Transactional
	@Modifying
	@Query(value = "update cinema_schedule CS "
    		+"inner join cinema C on CS.cinema_id = C.id "
    		+ "set CS.active =?1 "
    		+ "where C.active = 1 and C.id = ?2 ", nativeQuery = true)
    void updateScheduleActiveByCinemaId(boolean active, long id);
	
	
	@Transactional
	@Modifying
	@Query(value = "update cinema_schedule CS "
    		+"inner join cinema C on CS.cinema_id = C.id "
    		+"inner join cinema_auditorium CA on CS.auditorium_id = CA.id "
    		+ "set CS.active =?1 "
    		+ "where C.active = 1 and CA.active=1 and CS.auditorium_id = ?2 ", nativeQuery = true)
    void updateScheduleActiveByAuditoriumId(boolean active, long id);
	
	@Transactional
	@Modifying
	@Query(value = "update cinema_schedule CS "
    		+"inner join cinema C on CS.cinema_id = C.id "
    		+"inner join cinema_auditorium CA on CS.auditorium_id = CA.id "
    		+"inner join movie M on CS.movie_id = M.id "
    		+ "set CS.active =?1 "
    		+ "where C.active = 1 and M.active = 1 and CA.active=1 and M.id = ?2 ", nativeQuery = true)
    void updateScheduleActiveByMovieId(boolean active, long id);

	
}
