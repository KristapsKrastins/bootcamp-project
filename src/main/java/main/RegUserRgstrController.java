package main;

import java.security.Principal;
import java.util.List;
import java.util.logging.Logger;

import javax.validation.Valid;

import javax.persistence.RollbackException;
import org.hibernate.validator.internal.util.logging.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;

import main.User;
import main.UserRepository;

	@Controller 
	@RequestMapping(path="/user") 
	public class RegUserRgstrController {
		@Autowired
		
		private UserRepository uRepository;
		
		@GetMapping(path="/add")
		public @ResponseBody String addNewUser(@RequestParam String name,
				@RequestParam String email, @RequestParam String password) {
			User user = new User();
			user.setName(name);
			user.setEmail(email);
			user.setPassword(password);
			uRepository.save(user);
			return "Saved";
		}
		
		@GetMapping(path="/all")
		public @ResponseBody Iterable<User> getAllUsers(){
			return uRepository.findAll();
		}
		
		@RequestMapping(value = "/registration", method = RequestMethod.GET) 
		public String displayForm(Model model) { 
		    model.addAttribute("user", new User()); 
		    return "registration"; 
		}
		
		@RequestMapping(value = "/registration", method=RequestMethod.POST)
	    public String addNewUser(User user) {	
	        uRepository.save(user);
			Log.logger.debug("User has registered with id: " + user.getId() + " | name:" + user.getName() + " | email: " + user.getEmail());
	        return "redirect:/user/registration";
	        
	    }			
	}