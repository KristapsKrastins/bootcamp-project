package main;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import main.CinemaAuditorium;

public interface CinemaAuditoriumRepository extends CrudRepository<CinemaAuditorium, Long> {
	
	@Transactional
	@Modifying
    @Query("update cinema_auditorium c set c.cinemaId = ?1, c.name = ?2, c.seatsPerRow = ?3, c.rows = ?4 where c.id = ?5")
    void updateCinemaAuditorium(int cinemaId, String name,int seatsPerRow, int rows,  long id);
	
	public List <CinemaAuditorium> findByCinemaId(int id);
	
	@Transactional
	@Modifying
    @Query(value = "update cinema_auditorium c set c.active = ?1 where c.cinema_id = ?2", nativeQuery = true)
    void updateCinemaAuditoriumActiveByCinemaId(boolean active, long id);
	
	
	
	/*
	 update ud u
inner join sale s on
    u.id = s.udid
set u.assid = s.assid
	 */
	@Transactional
	@Modifying
    @Query(value = "update cinema_auditorium CA "
    		+"inner join cinema C on CA.cinema_id = C.id "
    		+ "set CA.active =?1 "
    		+ "where C.active = 1 and CA.id = ?2 ", nativeQuery = true)
    void updateCinemaAuditoriumActive(boolean active, long id);
	
}
