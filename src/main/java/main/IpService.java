package main;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;

import com.maxmind.geoip2.DatabaseReader;
import com.maxmind.geoip2.exception.GeoIp2Exception;
import com.maxmind.geoip2.model.CityResponse;

public class IpService {
    private DatabaseReader dbReader;
     
   
     
    public UserIp getLocation(InetAddress ip) 
      throws IOException, GeoIp2Exception {
        InetAddress ipAddress = InetAddress.getByName(ip.getHostName());
        CityResponse response = dbReader.city(ipAddress);
         
        String cityName = response.getCity().getName();
     
        return new UserIp(ip, "vpils");
    }
   
}
