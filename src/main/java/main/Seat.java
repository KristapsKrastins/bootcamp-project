package main;

import java.text.NumberFormat;
import java.util.Locale;

import javax.persistence.Embeddable;

@Embeddable
public class Seat {
	
	private Integer rowNum;

	private Integer seatNum;

	private int price = 3;
	
	private String priceFormatted;

	private boolean occupied = false;
	
	public Seat() {
		setPriceFormatted();
	}

	public Seat(int rowNum, int seatNum) {
		this.setRowNum(rowNum);
		this.setSeatNum(seatNum);
		setPriceFormatted();
	}

	public Seat(int rowNum, int seatNum, int price, boolean occupied) {
		this.setRowNum(rowNum);
		this.setSeatNum(seatNum);
//		this.setPrice(price);
//		this.setOccupied(occupied);
		setPriceFormatted();
	}

	public Integer getRowNum() {
		return rowNum;
	}

	public void setRowNum(Integer rowNum) {
		this.rowNum = rowNum;
	}

	public Integer getSeatNum() {
		return seatNum;
	}

	public void setSeatNum(Integer seatNum) {
		this.seatNum = seatNum;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public boolean isOccupied() {
		return occupied;
	}

	public void setOccupied(boolean occupied) {
		this.occupied = occupied;
	}

	public String getPriceFormatted() {
		return priceFormatted;
	}

	public void setPriceFormatted() {
		Locale locale = new Locale("lv", "LV");      
		NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance(locale);
		this.priceFormatted = currencyFormatter.format(price);
	}

}
