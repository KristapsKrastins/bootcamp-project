package main;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.Random;

import javax.validation.Valid;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import main.forms.*;

@Controller
@RequestMapping(path = "/admin")
public class AdminController {

	@Autowired
	private CinemaRepository cRepository;

	@Autowired
	private UserRepository uRepository;

	@Autowired
	private MovieRepository mRepository;

	@Autowired
	private PurchaseRepository pRepository;

	@Autowired
	private CinemaAuditoriumRepository aRepository;

	@Autowired
	private ScheduleRepository sRepository;

	private static final int PAGE_SIZE = 10;

	private static final String UPLOADED_FOLDER = "/images/";

	@GetMapping("")
	public String redirectToDashboard(Model model) {
		// public CinemaSchedule(Timestamp timeFrom, Timestamp timeTo, Cinema cinema,
		// CinemaAuditorium auditorium, Movie movie) {

		return "redirect:/admin/dashboard";
	}

	@GetMapping("/dashboard")
	public String dashboard(Model model) {
		model.addAttribute("active", "dashboard");
		return "admin";
	}

	@GetMapping(path = "/{active}/{page}")
	public String displayCinema(@PathVariable String active, @PathVariable int page, Model model) {
		// uRepository.save(new User());
		// || active != "cinemas" || active != "movies" || active != "screenings" ||
		// active != "purchases"
		if (active.equals("users") || active.equals("cinemas") || active.equals("movies") || active.equals("purchases")
				|| active.equals("auditoriums") || active.equals("schedules")) {
			model.addAttribute("active", active);
			switch (active) {
			case "cinemas":
				if (cRepository.count() > PAGE_SIZE)
					model.addAttribute("pages", (cRepository.count() / PAGE_SIZE) + 1);
				model.addAttribute("page", page);
				model.addAttribute("data", getItemsPerPage(cRepository.findAll(), page));
				model.addAttribute("keys", Cinema.getKeys());
				break;
			case "users":
				if (uRepository.count() > PAGE_SIZE)
					model.addAttribute("pages", (uRepository.count() / PAGE_SIZE) + 1);
				model.addAttribute("page", page);
				model.addAttribute("data", getItemsPerPage(uRepository.findAll(), page));
				model.addAttribute("keys", User.getKeys());
				break;
			case "movies":
				if (mRepository.count() > PAGE_SIZE)
					model.addAttribute("pages", (mRepository.count() / PAGE_SIZE) + 1);
				model.addAttribute("page", page);
				model.addAttribute("data", getItemsPerPage(mRepository.findAll(), page));
				model.addAttribute("keys", Movie.getKeys());
				break;
			case "purchases":
				if (pRepository.count() > PAGE_SIZE)
					model.addAttribute("pages", (pRepository.count() / PAGE_SIZE) + 1);
				model.addAttribute("page", page);
				model.addAttribute("data", getItemsPerPage(pRepository.findAll(), page));
				model.addAttribute("keys", Purchase.getKeys());
				break;
			case "auditoriums":
				if (aRepository.count() > PAGE_SIZE)
					model.addAttribute("pages", (aRepository.count() / PAGE_SIZE) + 1);
				model.addAttribute("page", page);
				model.addAttribute("data", getItemsPerPage(aRepository.findAll(), page));
				model.addAttribute("keys", CinemaAuditorium.getKeys());
				break;
			case "schedules":
				if (sRepository.count() > PAGE_SIZE)
					model.addAttribute("pages", (sRepository.count() / PAGE_SIZE) + 1);
				model.addAttribute("page", page);
				model.addAttribute("data", getItemsPerPage(sRepository.findAll(), page));
				model.addAttribute("keys", CinemaSchedule.getKeys());
				break;

			default:
				model.addAttribute("data", "Not cinema");
				break;
			}
		} else {
			return "redirect:/admin/dashboard";
		}

		return "admin";
	}

	// CINEMAS

	@GetMapping("/cinemas/new")
	public String showForm(CinemaForm cinemaForm, Model model) {
		model.addAttribute("active", "cinemas");
		return "forms/cinemaform";

	}

	@PostMapping("/cinemas/new")
	public String checkCinemaInfo(@RequestParam("file") MultipartFile file, RedirectAttributes redirectAttributes,
			@Valid CinemaForm cinemaForm, BindingResult bindingResult) {

		if (bindingResult.hasErrors()) {
			Log.logger.debug(bindingResult.getAllErrors());
			return "forms/cinemaform";
		}

		if (file.isEmpty()) {
			Log.logger.debug("No file in cinema new form");
			redirectAttributes.addFlashAttribute("message", "Please select a file to upload");
			return "forms/cinemaform";
		}

		String imgName = cinemaForm.getName().replaceAll("[^a-zA-Z0-9]", "") + getSaltString() + "."
				+ FilenameUtils.getExtension(file.getOriginalFilename());
		try {

			// Get the file and save it somewhere
			byte[] bytes = file.getBytes();

			Path path = Paths.get(UPLOADED_FOLDER + imgName);
			Files.write(path, bytes);

			redirectAttributes.addFlashAttribute("message",
					"You successfully uploaded '" + file.getOriginalFilename() + "'");

		} catch (IOException e) {
			e.printStackTrace();
		}
		
		cRepository.save(new Cinema(cinemaForm.getName(), cinemaForm.getCity(), cinemaForm.getAddress(), imgName));
		Log.logger.debug("New cinema has been made");
		return "redirect:/admin/cinemas/1";
	}

	@GetMapping("/cinemas/delete/{id}")
	public String deleteCinema(@PathVariable long id) {
		
		Optional<Cinema> oC = cRepository.findById(id);
		if (oC.isPresent()) {

			if (oC.get().isActive()) {
				aRepository.updateCinemaAuditoriumActiveByCinemaId(false, oC.get().getId());
				sRepository.updateScheduleActiveByCinemaId(false, oC.get().getId());
				cRepository.updateCinemaActive(false, id);
				Log.logger.debug("Cinema id:" +id +" set to false");
			}

			else {
				cRepository.updateCinemaActive(true, id);
				sRepository.updateScheduleActiveByCinemaId(true, oC.get().getId());
				aRepository.updateCinemaAuditoriumActiveByCinemaId(true, oC.get().getId());
				Log.logger.debug("Cinema id:" +id +" set to true");

			}

			// Goes through all

		} else {
			Log.logger.debug("Out of bounds");
			return "redirect:/admin/dashboard";
		}
		return "redirect:/admin/cinemas/1";
	}

	@GetMapping("/cinemas/edit/{id}")
	public String editCinema(@PathVariable long id, Model model, CinemaForm cinemaForm) {
		model.addAttribute("active", "cinemas");
		Optional<Cinema> c = cRepository.findById(id);
		if (c.isPresent()) {
			model.addAttribute("data", c.get());
			cinemaForm.setName(c.get().getName());
			cinemaForm.setAddress(c.get().getAddress());
			cinemaForm.setCity(c.get().getCity());
			return "forms/cinemaform";
		} else {
			Log.logger.debug("No cinema by id" + id);
			return "/";
		}

	}

	@PostMapping("/cinemas/edit/{id}")
	public String checkEditCinemaInfo(@RequestParam("file") MultipartFile file, @PathVariable long id,
			RedirectAttributes redirectAttributes, @Valid CinemaForm cinemaForm, BindingResult bindingResult) {

		Optional<Cinema> temp = cRepository.findById(id);
		if (!temp.isPresent()) {
			Log.logger.debug("No cinema by id" + id);
			return "redirect:/admin/cinemas/1";
		}

		if (bindingResult.hasErrors()) {
			Log.logger.debug(bindingResult.getAllErrors());
			return "forms/cinemaform";
			
		}
		String imgName = temp.get().getImgUrl();
		if (!file.isEmpty()) {
			try {

				// Get the file and save it somewhere
				byte[] bytes = file.getBytes();

				Path path = Paths.get(UPLOADED_FOLDER + imgName);
				Files.write(path, bytes);

				redirectAttributes.addFlashAttribute("message",
						"You successfully uploaded '" + file.getOriginalFilename() + "'");

			} catch (IOException e) {
				e.printStackTrace();
			}
		}

//        cRepository.findById(id).get().setCity(cinemaForm.getCity());
//        cRepository.findById(id).get().setName(cinemaForm.getName());
//        cRepository.findById(id).get().setAddress(cinemaForm.getAddress());

		cRepository.updateCinema(cinemaForm.getName(), cinemaForm.getCity(), cinemaForm.getAddress(), id);
		Log.logger.debug("Cinema with id:" + id + " updated.");
		return "redirect:/admin/cinemas/1";

	}

	// CINEMA END

	// Movies //

	@GetMapping("/movies/new")
	public String showMovieForm(MovieForm movieForm, Model model) {
		model.addAttribute("active", "movies");
		return "forms/movieform";

	}

	@PostMapping("/movies/new")
	public String checkMovieInfo(@RequestParam("file") MultipartFile file, RedirectAttributes redirectAttributes,
			@Valid MovieForm movieForm, BindingResult bindingResult) {
		
		if (bindingResult.hasErrors()) {
			Log.logger.debug(bindingResult.getAllErrors());
			return "forms/movieform";
			
		}

		if (file.isEmpty()) {
			Log.logger.debug("Movie file empty");
			redirectAttributes.addFlashAttribute("message", "Please select a file to upload");
			return "forms/movieform";
		}

		String imgName = movieForm.getName().replaceAll("[^a-zA-Z0-9]", "") + getSaltString() + "."
				+ FilenameUtils.getExtension(file.getOriginalFilename());
		try {

			// Get the file and save it somewhere
			byte[] bytes = file.getBytes();

			Path path = Paths.get(UPLOADED_FOLDER + imgName);
			Files.write(path, bytes);

			redirectAttributes.addFlashAttribute("message",
					"You successfully uploaded '" + file.getOriginalFilename() + "'");

		} catch (IOException e) {
			e.printStackTrace();
			Log.logger.debug(e);
		}

		// public Movie(String name, String description, String posterLink, String
		// trailerLink) {
		mRepository
				.save(new Movie(movieForm.getName(), movieForm.getDescription(), imgName, movieForm.getTrailerLink()));
		Log.logger.debug("New movie created.");
		return "redirect:/admin/movies/1";
	}

	@GetMapping("/movies/delete/{id}")
	public String deleteMovie(@PathVariable long id) {
		Optional<Movie> mC = mRepository.findById(id);
		if (mC.isPresent()) {
			if (mC.get().isActive()) {
				sRepository.updateScheduleActiveByMovieId(false, id);
				mRepository.updateMovieActive(false, id);
				Log.logger.debug("Movie with id:" + id + "Set to false");

			} else {

				mRepository.updateMovieActive(true, id);
				sRepository.updateScheduleActiveByMovieId(true, id);
				Log.logger.debug("Movie with id:" + id + "Set to true");

			}

		} else {
			Log.logger.debug("Movie does not exist with id:" + id );
			return "redirect:/admin/dashboard";
		}
		return "redirect:/admin/movies/1";
	}

	@GetMapping("/movies/edit/{id}")
	public String editMovie(@PathVariable long id, Model model, MovieForm movieForm) {
		model.addAttribute("active", "movies");
		Optional<Movie> m = mRepository.findById(id);
		if (m.isPresent()) {
			model.addAttribute("data", m.get());
			movieForm.setDescription(m.get().getDescription());
			movieForm.setName(m.get().getName());
			movieForm.setTrailerLink(m.get().getTrailerLink());
			return "forms/movieform";
		} else {
			Log.logger.debug("Movie with id:" + id + " does not exist");
			return "/";
		}

	}

	@PostMapping("/movies/edit/{id}")
	public String checkEditMovieInfo(@RequestParam("file") MultipartFile file, @PathVariable long id,
			RedirectAttributes redirectAttributes, @Valid MovieForm movieForm, BindingResult bindingResult) {

		Optional<Movie> temp = mRepository.findById(id);
		if (!temp.isPresent()) {
			Log.logger.debug("Movie with id:" + id + " Not found.");
			return "redirect:/admin/cinemas/1";
		}

		if (bindingResult.hasErrors()) {
			Log.logger.debug(bindingResult.getAllErrors());
			return "forms/cinemaform";
		}
		
		String imgName = temp.get().getPosterLink();
		if (!file.isEmpty()) {
			try {

				// Get the file and save it somewhere
				byte[] bytes = file.getBytes();

				Path path = Paths.get(UPLOADED_FOLDER + imgName);
				Files.write(path, bytes);

				redirectAttributes.addFlashAttribute("message",
						"You successfully uploaded '" + file.getOriginalFilename() + "'");

			} catch (IOException e) {
				e.printStackTrace();
			}
		}

//        cRepository.findById(id).get().setCity(cinemaForm.getCity());
//        cRepository.findById(id).get().setName(cinemaForm.getName());
//        cRepository.findById(id).get().setAddress(cinemaForm.getAddress());

		mRepository.updateMovie(movieForm.getName(), movieForm.getDescription(), imgName, movieForm.getTrailerLink(),
				id);
		Log.logger.debug("Movie with ID:" + id + " updated.");
		return "redirect:/admin/movies/1";

	}

	// Movies end

	// USERS START

	@GetMapping("/users/new")
	public String showUserForm(UserForm userForm, Model model) {
		model.addAttribute("active", "users");
		return "forms/userform";

	}

	@PostMapping("/users/new")
	public String checkUserInfo(@Valid UserForm userForm, BindingResult bindingResult) {

		if (bindingResult.hasErrors()) {
			Log.logger.debug(bindingResult.getAllErrors());
			return "forms/userform";
		}

		// public User(String name, String email, String password, Character role) {//

		uRepository.save(new User(userForm.getName(), userForm.getEmail(), userForm.getPassword(), userForm.getRole()));
		Log.logger.debug("New user created");
		return "redirect:/admin/users/1";
	}

	@GetMapping("/users/delete/{id}")
	public String deleteUser(@PathVariable long id) {
		Optional<User> uC = uRepository.findById(id);
		if (uC.isPresent()) {
			if (uC.get().isActive()) {
				uRepository.updateUserActive(false, id);
				Log.logger.debug("User with id:" +id+ " set to false.");
			} else {
				Log.logger.debug("User with id:" +id+ " set to true.");
			}
		} else {
			Log.logger.debug("In deleting user, user by id:" + id + "not found.");
			return "redirect:/admin/dashboard";
		}
		return "redirect:/admin/users/1";
	}

	@GetMapping("/users/edit/{id}")
	public String editUser(@PathVariable long id, Model model, UserForm userForm) {
		model.addAttribute("active", "users");
		Optional<User> u = uRepository.findById(id);
		if (u.isPresent()) {
			model.addAttribute("data", u.get());
			userForm.setName(u.get().getName());
			userForm.setEmail(u.get().getEmail());
			userForm.setRole(u.get().getRole());
			return "forms/userform";
		} else {
			Log.logger.debug("In editting user, user by id:" + id + "not found.");
			return "/";
		}

	}

	@PostMapping("/users/edit/{id}")
	public String editUserInfoCheck(@PathVariable long id, Model model, UserForm userForm,
			BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			Log.logger.debug(bindingResult.getAllErrors());
			return "forms/userform";	
		}

		// public User(String name, String email, String password, Character role) {//

		uRepository.updateUser(userForm.getEmail(), userForm.getName(), userForm.getRole(), id);
		Log.logger.debug("User with id:" + id + " updated.");
		return "redirect:/admin/users/1";

	}

	// USERS END

	// AUDITORIUM START

	@GetMapping("/auditoriums/new")
	public String showAuditoriumForm(AuditoriumForm auditoriumForm, Model model) {
		model.addAttribute("cinemas", cRepository.findAll());
		model.addAttribute("active", "auditoriums");
		return "forms/auditoriumform";

	}

	@PostMapping("/auditoriums/new")
	public String checkAuditoriumInfo(@Valid AuditoriumForm auditoriumForm, RedirectAttributes redirectAttributes,
			BindingResult bindingResult) {

		if (bindingResult.hasErrors()) {
			Log.logger.debug(bindingResult.getAllErrors());
			return "forms/auditoriumform";
		}
		Optional<Cinema> oC = cRepository.findById((long) auditoriumForm.getCinemaId());
		if (!oC.isPresent()) {
			redirectAttributes.addFlashAttribute("errorMessage", "No cinema by this id found");
			Log.logger.debug("No cinema with id:" + oC.get().getId() + "found in auditorium form.");
			return "redirect:/admin/auditoriums/new";
		}

		aRepository.save(new CinemaAuditorium(auditoriumForm.getCinemaId(), auditoriumForm.getName(),
				auditoriumForm.getRows(), auditoriumForm.getSeatsPerRow()));
		Log.logger.debug("New cinema auditorium added");
		return "redirect:/admin/auditoriums/1";
	}

	@GetMapping("/auditoriums/delete/{id}")
	public String deleteAuditorium(@PathVariable long id) {
		Optional<CinemaAuditorium> aC = aRepository.findById(id);
		if (aC.isPresent()) {
			if (aC.get().isActive()) {
				sRepository.updateScheduleActiveByAuditoriumId(false, id);
				aRepository.updateCinemaAuditoriumActive(false, id);
				Log.logger.debug("Auditorium with ID: " + id + " set to false.");
			} else {
				aRepository.updateCinemaAuditoriumActive(true, id);
				sRepository.updateScheduleActiveByAuditoriumId(true, id);
				Log.logger.debug("Auditorium with ID: " + id + " set to true.");
			}

		} else {
			Log.logger.debug("No auditorium by ID: " + id + " found.");
			return "redirect:/admin/dashboard";
		}
		return "redirect:/admin/auditoriums/1";
	}

	@GetMapping("/auditoriums/edit/{id}")
	public String editAuditoriums(@PathVariable long id, Model model, AuditoriumForm auditoriumForm) {
		model.addAttribute("active", "auditoriums");
		Optional<CinemaAuditorium> cM = aRepository.findById(id);
		if (!cM.isPresent()) {
			Log.logger.debug("No auditorium by ID: " + id + " found in edit form.");
			return "redirect:/admin/auditoriums/1";
		}
		model.addAttribute("cinemas", cRepository.findAll());
		model.addAttribute("data", cM.get());
		auditoriumForm.setName(cM.get().getName());
		auditoriumForm.setCinemaId(cM.get().getCinemaId());
		auditoriumForm.setRows(cM.get().getRows());
		auditoriumForm.setSeatsPerRow(cM.get().getSeatsPerRow());
		return "forms/auditoriumform";
	}

	@PostMapping("/auditoriums/edit/{id}")
	public String checkEditAuditoriumInfo(@Valid AuditoriumForm auditoriumForm, @PathVariable long id,
			RedirectAttributes redirectAttributes, BindingResult bindingResult) {

		if (bindingResult.hasErrors()) {
			Log.logger.debug(bindingResult.getAllErrors());
			return "forms/auditoriumform";
		}
		Optional<Cinema> oC = cRepository.findById((long) auditoriumForm.getCinemaId());
		if (!oC.isPresent()) {
			Log.logger.debug("Cinema by ID:" + oC.get().getId() + "found for auditorium" + id);
			redirectAttributes.addFlashAttribute("errorMessage", "No cinema by this id found");
			return "redirect:/admin/auditoriums/new";
		}

		aRepository.updateCinemaAuditorium(auditoriumForm.getCinemaId(), auditoriumForm.getName(),
				auditoriumForm.getSeatsPerRow(), auditoriumForm.getRows(), id);
		Log.logger.debug("Cinema by ID:" + oC.get().getId() + "found for auditorium" + id);
		return "redirect:/admin/auditoriums/1";
	}

	// AUDITORIUM END

	// SCHEDULE START

	@GetMapping("/schedules/new")
	public String showScheduleForm(ScheduleForm scheduleForm, Model model) {
		model.addAttribute("active", "schedules");
		model.addAttribute("cinemas", cRepository.findAll());
		model.addAttribute("movies", mRepository.findAll());
		return "forms/scheduleform";

	}

	@PostMapping("/schedules/new")
	public String checkScheduleInfo(@Valid ScheduleForm scheduleForm, RedirectAttributes redirectAttributes,
			BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			System.out.println("Something wrong");
			return "forms/scheduleform";
		}

		Optional<Cinema> oC = cRepository.findById((long) scheduleForm.getCinemaID());
		if (!oC.isPresent()) {
			redirectAttributes.addFlashAttribute("errorMessage", "No cinema by this id found");
			return "redirect:/admin/schedules/new";
		}
		Optional<CinemaAuditorium> oA = aRepository.findById((long) scheduleForm.getAuditoriumID());
		if (!oA.isPresent()) {
			redirectAttributes.addFlashAttribute("errorMessage", "No Auditorium by this id found");
			return "redirect:/admin/schedules/new";
		}
		if (oA.get().getCinemaId() != oC.get().getId()) {
			redirectAttributes.addFlashAttribute("errorMessage",
					"This auditorium does not belong to the specified cinema");
			return "redirect:/admin/schedules/new";
		}
		Optional<Movie> oM = mRepository.findById((long) scheduleForm.getMovieID());
		if (!oM.isPresent()) {
			redirectAttributes.addFlashAttribute("errorMessage", "No Movie by this id found");
			return "redirect:/admin/schedules/new";
		}
		Timestamp timeFrom = new Timestamp(System.currentTimeMillis() + 10);
		Timestamp timeTo = new Timestamp(System.currentTimeMillis() + 11);
		try {
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			Date timeF = dateFormat.parse(scheduleForm.getDate() + " " + scheduleForm.getTimeFrom() + ":00");
			Date timeT = dateFormat.parse(scheduleForm.getDate() + " " + scheduleForm.getTimeTo() + ":00");
			timeFrom = new Timestamp(timeF.getTime());
			timeTo = new Timestamp(timeT.getTime());
		} catch (Exception e) { // this generic but you can control another types of exception
			System.out.print(e);
		}

		if (timeFrom.after(timeTo)) {
			redirectAttributes.addFlashAttribute("errorMessage", "From cannot be larger than to");
			return "redirect:/admin/schedules/new";
		}
		if (timeFrom.before(new Timestamp(System.currentTimeMillis()))) {
			redirectAttributes.addFlashAttribute("errorMessage", "Can't set schedule to past date");
			return "redirect:/admin/schedules/new";
		}

		// public CinemaSchedule(Timestamp dateTime, Cinema cinema, CinemaAuditorium
		// auditorium, Movie movie) {
		// aRepository.save(new
		// CinemaAuditorium(auditoriumForm.getCinemaId(),auditoriumForm.getName(),auditoriumForm.getRows(),auditoriumForm.getSeatsPerRow()));
		sRepository.save(
				new CinemaSchedule(timeFrom, timeTo, aRepository.findById((long) scheduleForm.getAuditoriumID()).get(),
						cRepository.findById((long) scheduleForm.getCinemaID()).get(),
						mRepository.findById((long) scheduleForm.getMovieID()).get()));
		return "redirect:/admin/schedules/1";
	}

	@GetMapping("/schedules/delete/{id}")
	public String deleteSchedule(@PathVariable long id) {
		Optional<CinemaSchedule> sC = sRepository.findById(id);
		if (sC.isPresent()) {
			if (sC.get().isActive()) {
				sRepository.updateScheduleActive(false, id);
			} else {
				sRepository.updateScheduleActive(true, id);
			}

		} else {
			return "redirect:/admin/dashboard";
		}
		return "redirect:/admin/schedules/1";
	}

	@GetMapping("/schedules/edit/{id}")
	public String editSchedule(@PathVariable long id, Model model, ScheduleForm scheduleForm) {
		model.addAttribute("active", "schedules");
		Optional<CinemaSchedule> cS = sRepository.findById(id);
		if (!cS.isPresent())
			return "redirect:/admin/schedules/1";
		model.addAttribute("cinemas", cRepository.findAll());
		model.addAttribute("movies", mRepository.findAll());
		model.addAttribute("data", cS.get());
		scheduleForm.setAuditoriumID((int) cS.get().getAuditorium().getId());
		scheduleForm.setCinemaID((int) cS.get().getCinema().getId());
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		
		
		String timeFrom = "", timeTo = "", date = "";
		if(cS.get().getTimeFrom().getMonth() < 10 && cS.get().getTimeFrom().getDay() < 10) {
			date =(cS.get().getTimeFrom().getYear()+1900) +"-"+ "0"+cS.get().getTimeFrom().getMonth() +"-0"+ cS.get().getTimeFrom().getDay() ;
		}
		else if( cS.get().getTimeFrom().getMonth() < 10) {
			 date =(cS.get().getTimeFrom().getYear()+1900) +"-"+ "0"+cS.get().getTimeFrom().getMonth() +"-"+ cS.get().getTimeFrom().getDay() ;
		}else if(cS.get().getTimeFrom().getDay() < 10) {
			 date =(cS.get().getTimeFrom().getYear()+1900) +"-"+ cS.get().getTimeFrom().getMonth() +"-0"+ cS.get().getTimeFrom().getDay();
		}
		else {
			 date =(cS.get().getTimeFrom().getYear()+1900) +"-"+ cS.get().getTimeFrom().getMonth() +"-"+ cS.get().getTimeFrom().getDay()  ;
		}
		
		
		
		
		if (cS.get().getTimeFrom().getHours() < 10) {
			timeFrom = "0" + cS.get().getTimeFrom().getHours() + ":" + cS.get().getTimeFrom().getMinutes();
		} else if (cS.get().getTimeFrom().getMinutes() < 10) {
			timeFrom = cS.get().getTimeFrom().getHours() + ":0" + cS.get().getTimeFrom().getMinutes();
		}else if(cS.get().getTimeFrom().getHours() < 10 && cS.get().getTimeFrom().getMinutes() < 10)
			timeFrom = "0" +cS.get().getTimeFrom().getHours() + ":0" + cS.get().getTimeFrom().getMinutes();
		else {
			timeFrom = cS.get().getTimeFrom().getHours() + ":" + cS.get().getTimeFrom().getMinutes();
		}
		// String timeFrom = cS.get().getTimeFrom().getHours() + ":" +
		// cS.get().getTimeFrom().getMinutes();
		if (cS.get().getTimeTo().getHours() < 10) {
			timeTo = "0" + cS.get().getTimeTo().getHours() + ":" + cS.get().getTimeTo().getMinutes();
		} else if (cS.get().getTimeTo().getMinutes() < 10) {
			timeTo = cS.get().getTimeTo().getHours() + ":0" + cS.get().getTimeTo().getMinutes();
		}
		else if(cS.get().getTimeTo().getHours() < 10 && cS.get().getTimeTo().getHours() < 10) {
			timeTo = "0" +cS.get().getTimeTo().getHours() + ":0" + cS.get().getTimeTo().getMinutes();
		}
		 else {
			timeTo = cS.get().getTimeTo().getHours() + ""+ cS.get().getTimeTo().getMinutes();
		}

		scheduleForm.setTimeTo(timeTo);
		scheduleForm.setTimeFrom(timeFrom);
		scheduleForm.setDate(date);
		return "forms/scheduleform";
	}

	@GetMapping("/schedules/cinema/{id}")
	public @ResponseBody List<CinemaAuditorium> findAllAgencies(@PathVariable int id) {
		return aRepository.findByCinemaId(id);
	}

	// SCHEDULE END

	// HELP FUNCTIONS

	private <T> ArrayList<T> getItemsPerPage(Iterable<T> list, int page) {
		ArrayList<T> temp = new ArrayList<T>();
		ArrayList<T> result = new ArrayList<T>();
		Iterator<T> iterator = list.iterator();

		while (iterator.hasNext())
			temp.add(iterator.next());

		int inc = 0;

		for (T item : temp) {
			if (inc >= (PAGE_SIZE) * (page - 1) && inc < (PAGE_SIZE) * (page)) {
				result.add(item);

			}
			inc++;
		}

		return result;
	}

	protected String getSaltString() {
		String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
		StringBuilder salt = new StringBuilder();
		Random rnd = new Random();
		while (salt.length() < 7) { // length of the random string.
			int index = (int) (rnd.nextFloat() * SALTCHARS.length());
			salt.append(SALTCHARS.charAt(index));
		}
		String saltStr = salt.toString();
		return saltStr;

	}

}