package main;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.springframework.data.annotation.Transient;


@Entity(name="user")
public class User {
	@Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE)
    private long id;

    private String name;

    private String email;
    @Transient
    private String password;
    

    
    
    private Character role;

    private boolean active;
  
    public User(String name, String email, String password, Character role) {
    	this.name = name;
    	this.email = email;
    	this.password = password;
    	this.role = role;
    	this.active = true;
    }
    
    public User() {
    	this.name = "";
    	this.email = "";
    	this.password = "";
    	this.role = 'u';
    	this.active = true;
    }
    public User(String name, String email, String password, char role) {
    	setName(name);
    	setEmail(email);
    	setPassword(password);
    	setRole(role);
    	this.active = true;
    }

    
	public long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		if (name != null && !name.isEmpty()) {
			this.name = name;
		}else {
			this.name = "Not set";
		}
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		Pattern pattern = Pattern.compile("^.+@.+\\..+$");
		Matcher matcher = pattern.matcher(this.email = email);
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}

	public Character getRole() {
		return role;
	}
	
	public void setRole(Character role) {
		this.role = role;
	}
	public static String[] getKeys(){
		return new String[] {"id", "name", "email","role"};
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}
}
