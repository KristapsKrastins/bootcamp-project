package main;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Random;
import java.util.StringTokenizer;

import javax.print.DocFlavor.URL;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import com.maxmind.geoip2.DatabaseReader;
import com.maxmind.geoip2.exception.GeoIp2Exception;
import com.maxmind.geoip2.model.CityResponse;

@Controller
@RequestMapping(path="/")
public class IndexController {
	//private static final Logger logger = Logger.getLogger(WelcomeController.class);

	 @Autowired
	  private CinemaRepository cRepository;
	 
	 private IpService locationService;
	 
	    public IndexController() throws IOException {
	        locationService = new IpService();
	    }
	     
	    public static String getClientIpAddress(HttpServletRequest request) {
	        String xForwardedForHeader = request.getHeader("X-Forwarded-For");
	        if (xForwardedForHeader == null) {
	            return request.getRemoteAddr();
	        } else {
	    
	            return new StringTokenizer(xForwardedForHeader, ",").nextToken().trim();
	        }
	    }
	    public InetAddress presetIp;
	    public String filter;

	    @GetMapping("/")
	    public String getLocation(HttpServletRequest ipRequest, Model model) 
	    
	    {	

	    	getClientIpAddress(ipRequest);  	

	    	if (ipRequest.getRemoteAddr().equals("127.0.0.1")) {
	    		
	    		try {
					presetIp = InetAddress.getByName("193.105.155.7");
	//				System.out.println(presetIp);
				} catch (UnknownHostException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					
				}
	    		
	    	}else {
	    		try {
					presetIp = InetAddress.getByName(ipRequest.getRemoteAddr());
	//				System.out.println(presetIp);
				} catch (UnknownHostException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	    		
	    		
	    	}
	  	
	    	

	 	    
	    	String dbLocation = null;
			try {
				dbLocation = new File("GeoLite2-City.mmdb").getCanonicalPath();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}    	
	
	    	//	System.out.println(dbLocation);
	    		File database = new File(dbLocation);
	    	
	    		
	    			
	    		
	    		DatabaseReader dbReader;
	    		try {
	    			dbReader = new DatabaseReader.Builder(database).build();
	    			CityResponse response = dbReader.city(presetIp);
	    			filter = (response.getCity().getName());
	    			
	    			
	    		} catch (IOException | GeoIp2Exception e1) {
	    			// TODO Auto-generated catch block
	    			e1.printStackTrace();
	    		}
	    		
	 //   		System.out.print(filter);
	    		
//	    		cRepository.deleteAll();
//	       
//	    		cRepository.save(new Cinema("First","London","Sezame street 5","cinema.jpg"));
//	        	cRepository.save(new Cinema("Second","Riga","City street 5","cinema.jpg"));
//	        	cRepository.save(new Cinema("Third","Ventspils","England street 5","cinema.jpg"));
//	        	cRepository.save(new Cinema("Fourth","Ventspils","Sezame street 44","cinema.jpg"));
//	    		cRepository.save(new Cinema("Fifth","Ventspils","Saules street 114","cinema.jpg"));
//	    		cRepository.save(new Cinema("Sixth","New York","Lincoln Street 68","cinema.jpg"));
	    		
	    		
	    		
	    		List <Cinema> cinemaList = (List<Cinema>) cRepository.findAll() ;
	    		
	    		//identifier = (target_type) value
	    		
	    		List <Cinema> fiteredCinemas = cRepository.findByCity(filter);
	    		
	    		if (!fiteredCinemas.isEmpty()) {
    		
	    		
	    		
	    		model.addAttribute("cinemas",fiteredCinemas);
	    	//	System.out.println(allCinemas);
	    		
	    		}else {
	    		    List<Cinema> randomElement = new ArrayList<Cinema>();

	    		
	    			
	    		for (int i = 0; i < 3; i++) {
		    		Random rand = new Random();
		    		randomElement.add(cinemaList.get(rand.nextInt(cinemaList.size())));
	    		}
	    		model.addAttribute("cinemas",randomElement);
	    	//	System.out.println(randomElement);
	    			
	    			
	    		}
	    	
	        return "index";
	        
	     

	 
	 
	 
	
//    @GetMapping("/")
//
// //   public String anything(@RequestParam(name="location", required=false, defaultValue="all") String location, Model model) {
////    	cRepository.deleteAll();
////    
////    	cRepository.save(new Cinema("First","London","Sezame street 5","cinema1.jpg"));
////    	cRepository.save(new Cinema("Second","Riga","City street 5","cinema1.jpg"));
////    	cRepository.save(new Cinema("Third","Ventspils","England street 5","cinema1.jpg"));
////    	cRepository.save(new Cinema("Fourth","Ventspils","Sezame street 44","cinema1.jpg"));
//    	
////   	
//    	//cRepository.save(new Cinema("Fourth","Ventspils","Sezame street 44","cinema1.jpg"));
//
// 
//    	model.addAttribute("cinemas", cRepository.findAll());
//    	model.addAttribute("location", location);
//    	
    	
    	
    	
    	
    

    
    }
}
	    		
    
    
    
    
    


	    