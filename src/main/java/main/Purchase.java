package main;

import java.sql.Timestamp;
import java.text.NumberFormat;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

import javax.persistence.CollectionTable;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name="purchase")
public class Purchase{
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE)
    private long id;
    
	@ManyToOne
	@JoinColumn(name = "userId")
	private User user;

	@ManyToOne
	@JoinColumn(name = "scheduleId")
	private CinemaSchedule schedule;

	@ElementCollection(fetch=FetchType.EAGER)
	@CollectionTable(name = "purchased_seats", joinColumns = @JoinColumn(name = "id"))
	private Set<Seat> seats = new HashSet<Seat>();

	private double totalAmount;
	
	private String totalAmountFormatted; 

	private char status;

	private Timestamp lastActionDateTime;

	private String gatewayTransactionId;
	
	private boolean active = true;

	@SuppressWarnings("unused")
	private Purchase() {

	}

	public Purchase(User user, CinemaSchedule schedule, Set<Seat> seats) {
		setUser(user);
		setSchedule(schedule);
		setSeats(seats);
		setStatus('r');
		setTotalAmount();
		// Gateway will probably be added after purchase, maybe...
	}

	public Long getId() {
		return id;
	}

	public char getStatus() {
		return status;
	}

	public void setStatus(char status) {
		setLastActionDateTime(new Timestamp(System.currentTimeMillis()));
		this.status = status;
	}

	public Timestamp getLastActionDateTime() {
		return lastActionDateTime;
	}

	public void setLastActionDateTime(Timestamp lastActionDateTime) {
		this.lastActionDateTime = lastActionDateTime;
	}

	public String getGatewayTransactionId() {
		return gatewayTransactionId;
	}

	public void setGatewayTransactionId(String gatewayTransactionId) {
		this.gatewayTransactionId = gatewayTransactionId;
	}

	public Set<Seat> getSeats() {
		return seats;
	}

	public void setSeats(Set<Seat> seats) {
		this.seats = seats;
	}
	
	
	
    
    public static String[] getKeys(){
		return new String[] { "gatewayTransactionId"};
	}
    

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount() {
		this.totalAmount = 0;
		for (Seat seat : seats) {
			this.totalAmount += seat.getPrice();
		}
		setTotalAmountFormatted();
	}

	public CinemaSchedule getSchedule() {
		return schedule;
	}

	public void setSchedule(CinemaSchedule schedule) {
		this.schedule = schedule;
	}

	public String getTotalAmountFormatted() {
		return totalAmountFormatted;
	}

	public void setTotalAmountFormatted() {
		Locale locale = new Locale("lv", "LV");      
		NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance(locale);
		this.totalAmountFormatted = currencyFormatter.format(totalAmount);
	}

	public boolean getActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

}
