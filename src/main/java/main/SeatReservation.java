package main;

import java.sql.Timestamp;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class SeatReservation {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "screeningId")
	private CinemaSchedule screening;

	@Embedded
	private Seat seat;

	@ManyToOne
	@JoinColumn(name = "purchaseId")
	private Purchase purchase;

	@SuppressWarnings("unused")
	private SeatReservation() {
	}

	public SeatReservation(CinemaSchedule screening, Seat seat, Purchase purchase) {
		this.screening = screening;
		this.seat = seat;
		this.purchase = purchase;
	}

	public CinemaSchedule getScreening() {
		return screening;
	}

	public void setScreening(CinemaSchedule screening) {
		this.screening = screening;
	}

	public Seat getSeat() {
		return seat;
	}

	public void setSeat(Seat seat) {
		this.seat = seat;
	}

	public Purchase getPurchase() {
		return purchase;
	}

	public void setPurchase(Purchase purchase) {
		this.purchase = purchase;
	}

}
