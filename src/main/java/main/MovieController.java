package main;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import main.Movie;
import main.MovieRepository;

	@Controller 
	@RequestMapping(path="/cinema/{cinemaId}/movie") 
	public class MovieController {
		
		@Autowired
		private MovieRepository movieRepository;
		@Autowired
		private ScheduleRepository scheduleRepository;
		@Autowired
		private CinemaRepository cinemaRepository;
		
		//ADD MOVIE
		
		@GetMapping(path="/add")
		public @ResponseBody String addNewMovie(@RequestParam String name, @RequestParam String description,
				@RequestParam String posterLink, @RequestParam String trailerLink) {
			Movie movie = new Movie(name, description, posterLink, trailerLink);
			Cinema cinema = new Cinema(); /////////
			cinema.getId();				/////////
			movie.setName(name);	
			movie.setDescription(description);
			movie.setPosterLink(posterLink);
			movie.setTrailerLink(trailerLink);
			movieRepository.save(movie);
			return "Saved";
		}
		
		@GetMapping(path = "/all")
		public @ResponseBody Iterable<Movie> getAllMovies() {
			return movieRepository.findAll();
		}
		
		//SHOW DESCRIPTION AND OTHER STUFF FOR SPECIFIC MOVIE
		
		@GetMapping(path="/{movieId}")
		public String displayCinema(@PathVariable long movieId,Model model) {
	    	  	
	
			Optional<Movie> movieSearch = movieRepository.findById(movieId);
			
			if(movieSearch.isPresent()) {
				
				Movie movie = movieSearch.get();
				model.addAttribute("movieName", movie.getName());
				model.addAttribute("movieDescription", movie.getDescription());
				model.addAttribute("moviePoster", movie.getPosterLink());
				model.addAttribute("movieTrailer", movie.getTrailerLink());	
			    
				return "fragments/movie";
			}
			else {
				throw new RuntimeException("Movies not found!");
			}
		
		}
		
		
		
		
		// need cinema and schedule
		
		
		
	    
}

