package main;

import static org.junit.Assert.*;

import org.junit.Test;

public class SeatTest {

	@Test
	public void createSeat() {
		
		Integer rowNum = 5;
		Integer seatNum = 25;
		int price = 3;
		String priceFormatted = "string";
		boolean occupied = false;
		
		Seat seat = new Seat(rowNum, seatNum, price, occupied);
		
	//	assertEquals("Row number does not match the given parameter", rowNum, seat.getRowNum());
	//	assertEquals("Seat number does not match the given parameter", seatNum, seat.getSeatNum());
		assertEquals("Price does not match the given parameter", price, seat.getPrice());
		assertEquals("Boolean does not match the given parameter", occupied, seat.isOccupied());
	}
	


}
